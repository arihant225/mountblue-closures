function cacheFunction(cb, cached) {
    let result = undefined
    if (result == undefined) {
        if (typeof cb == 'function') {
            result = cb(...cached)
        }
        return function () {
            return result
        }
    }
}

module.exports = cacheFunction
