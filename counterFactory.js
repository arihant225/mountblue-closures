function counterFactory(counterVariable) {
    function increment() {
        counterVariable = counterVariable + 1
        return counterVariable
    }

    function decrement() {
        counterVariable = counterVariable - 1
        return counterVariable
    }

    return { increment: increment, decrement: decrement }
}

module.exports = counterFactory

