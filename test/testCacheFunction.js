const cacheFunction = require("../cacheFunction");


function add(x,y,z){

 return x+y+z
}

let fn = cacheFunction(add,[10,20,40])
console.log(fn()) // invokes functions

console.log(fn()) // don't invoke function

function multiply(a,b)
{
    
    return a*b
}
let newfn=cacheFunction(multiply,[10,20])
console.log(newfn())  //invokes function

console.log(newfn()) //don't invoke function