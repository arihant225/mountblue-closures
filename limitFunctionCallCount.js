function limitFunctionCallCount(cb = null, n = null) {
    if ((typeof cb == 'function') && ((typeof n == 'number') && (n > 0))) {
        return function () {
            for (let counterVariable = 1; counterVariable <= n; counterVariable++) {
                try {
                    cb()
                }
                catch {
                    return null
                }
            }
        }
    }
    return function () {
        return null
    }
}

module.exports = limitFunctionCallCount